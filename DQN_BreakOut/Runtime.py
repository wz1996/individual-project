import time

import torch
import torch.nn as nn
import torch.nn.functional as F
from atari_wrappers import make_atari, wrap_deepmind

class DQN(nn.Module):
    def __init__(self, in_channels=4, num_actions=5):

        super(DQN, self).__init__()
        self.conv1 = nn.Conv2d(in_channels, 32, kernel_size=8, stride=4)
        self.conv2 = nn.Conv2d(32, 64, kernel_size=4, stride=2)
        self.conv3 = nn.Conv2d(64, 64, kernel_size=3, stride=1)
        self.fc4 = nn.Linear(7 * 7 * 64, 512)
        self.fc5 = nn.Linear(512, num_actions)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))
        x = F.relu(self.fc4(x.reshape(x.size(0), -1)))
        return self.fc5(x)

network=torch.load('network-breakout-test1.pkl')


env = make_atari('BreakoutNoFrameskip-v4')
env = wrap_deepmind(env, scale=False, frame_stack=True)
obersevation=env.reset()
while True:
    env.render()
    state = torch.from_numpy(obersevation._force().transpose(2, 0, 1)[None] / 255).float()
    state = state.cuda()
    Q_value=network(state)
    action = Q_value.argmax(1)[0]
    next_observation, _, done, _ = env.step(action)
    obersevation=next_observation
    time.sleep(.1)
